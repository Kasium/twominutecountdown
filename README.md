# TwoMinuteCountdown

A simple countdown timer app that starts with an initial value of 2 minutes and
counts down to zero.

## Detailed specifications
- A label that shows the remaining time in Minutes, seconds and one digit for hundred
milliseconds.
- If number of minutes is zero, it displays only seconds and one digit for hundred
milliseconds
- A "+" button. When clicked, this button increases the time remaining by 10 seconds,
up to a maximum of 2 minutes.
- If the countdown has expired (i.e., after the countdown hits 0 ms), then display the
message "DONE"
- The countdown should continue if the user opens another app.
- If the app is killed and started again, timer starts with initial value

## Minor beautification
- Add text color with down state for button
- Make portrait possible again with the right dimensions and correct rotation handling
