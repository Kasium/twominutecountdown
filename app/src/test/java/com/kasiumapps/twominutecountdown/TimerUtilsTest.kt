package com.kasiumapps.twominutecountdown

import com.kasiumapps.twominutecountdown.utils.TimerUtils
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TimerUtilsTest {

    @Test
    fun minutesIsCorrect() {
        assertEquals("2 : ", TimerUtils.getMinutes(TimerUtils.TWO_MINUTES))
        assertEquals("1 : ", TimerUtils.getMinutes(63000))
        assertEquals("", TimerUtils.getMinutes(59000))
    }

    @Test
    fun secondsIsCorrect() {
        assertEquals(0L, TimerUtils.getSeconds(TimerUtils.TWO_MINUTES))
        assertEquals(1L, TimerUtils.getSeconds(121350))
        assertEquals(11L, TimerUtils.getSeconds(71350))
    }

    @Test
    fun hundredsIsCorrect() {
        assertEquals(0L, TimerUtils.getHundreds(TimerUtils.TWO_MINUTES))
        assertEquals(3L, TimerUtils.getHundreds(121360))
        assertEquals(1L, TimerUtils.getHundreds(71100))
    }

    @Test
    fun addTenSecondsBelowTwoMinutes() {
        val millis = 1000L
        assertEquals(millis + 10000L, TimerUtils.addTenSeconds(millis))
    }

    @Test
    fun addTenSecondsOverTwoMinutes() {
        val millis = 119999L
        assertEquals(120000L, TimerUtils.addTenSeconds(millis))
    }
}
