package com.kasiumapps.twominutecountdown.utils

import android.content.Context
import android.text.format.DateUtils
import com.kasiumapps.twominutecountdown.R

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */

class TimerUtils {
    companion object {
        const val TWO_MINUTES = 2 * DateUtils.MINUTE_IN_MILLIS
        const val TEN_SECONDS = 10 * DateUtils.SECOND_IN_MILLIS

        fun formatTime(millis: Long, context: Context): String {
            return if (millis <= 0) {
                context.getString(R.string.done)
            } else {
                String.format("${getMinutes(millis)}%02d : ${getHundreds(millis)}", getSeconds(millis))
            }
        }

        fun getMinutes(millis: Long): String {
            return if ((millis / DateUtils.MINUTE_IN_MILLIS) == 0L) {
                ""
            }else {
                "${millis / DateUtils.MINUTE_IN_MILLIS} : "
            }
        }

        fun getSeconds(millis: Long) = (millis % DateUtils.MINUTE_IN_MILLIS) / DateUtils.SECOND_IN_MILLIS

        fun getHundreds(millis: Long) =  (millis % DateUtils.SECOND_IN_MILLIS) / 100

        fun addTenSeconds(millis: Long) =
            if (millis + TEN_SECONDS >= TWO_MINUTES) TWO_MINUTES
            else millis + TEN_SECONDS
    }
}