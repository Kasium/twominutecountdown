package com.kasiumapps.twominutecountdown

import android.app.Application
import com.kasiumapps.twominutecountdown.di.AppComponent
import com.kasiumapps.twominutecountdown.di.DaggerAppComponent

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class TimerApplication: Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = initDagger()
    }

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .build()
}