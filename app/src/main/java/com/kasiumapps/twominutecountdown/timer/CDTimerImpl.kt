package com.kasiumapps.twominutecountdown.timer

import android.os.CountDownTimer

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class CDTimerImpl : CDTimer {
    private var millis: Long? = null
    private var countDownTimer: CountDownTimer? = null

    override fun initWithMillis(millis: Long, listener: CDTimer.Listener) {
        cancel()
        this.millis = millis
        countDownTimer = object: CountDownTimer(millis, 100) {
            override fun onFinish() {
                listener.onFinish()
            }

            override fun onTick(millisUntilFinished: Long) {
                listener.onTick(millisUntilFinished)
            }
        }.start()
    }

    private fun cancel() {
        countDownTimer?.cancel()
        countDownTimer = null
    }
}