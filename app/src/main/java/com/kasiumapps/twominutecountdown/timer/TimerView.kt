package com.kasiumapps.twominutecountdown.timer

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
interface TimerView {
    var textAlignment: Int
    fun setText(text: String)
}