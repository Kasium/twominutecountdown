package com.kasiumapps.twominutecountdown.timer

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */

interface CDTimer {
    fun initWithMillis(millis: Long, listener: Listener)
    interface Listener {
        fun onFinish()
        fun onTick(millisUntilFinished: Long)
    }
}