package com.kasiumapps.twominutecountdown.timer

import android.content.Context
import android.view.View
import com.kasiumapps.twominutecountdown.timer.CDTimer.Listener
import com.kasiumapps.twominutecountdown.utils.TimerUtils

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class TimerPresenter(var timer: CDTimer, val timerView: TimerView, val context: Context,
                     var timeLeft: Long = TimerUtils.TWO_MINUTES): Listener {

    fun startCountDownTimer() {
        timerView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        timer.initWithMillis(timeLeft, this)
    }

    override fun onFinish() {
        timeLeft = 0
        timerView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        timerView.setText(TimerUtils.formatTime(timeLeft, context))
    }

    override fun onTick(millisUntilFinished: Long) {
        timeLeft = millisUntilFinished
        timerView.setText(TimerUtils.formatTime(timeLeft, context))
    }
}