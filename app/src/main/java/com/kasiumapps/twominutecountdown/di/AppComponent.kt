package com.kasiumapps.twominutecountdown.di

import com.kasiumapps.twominutecountdown.timer.CDTimer
import dagger.Component

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
@Component(modules = [AppModule::class])
interface AppComponent {
    fun cdTimer(): CDTimer
}