package com.kasiumapps.twominutecountdown.di

import com.kasiumapps.twominutecountdown.timer.CDTimer
import com.kasiumapps.twominutecountdown.timer.CDTimerImpl
import dagger.Module
import dagger.Provides

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
@Module
class AppModule {
    @Provides
    fun cdTimer(): CDTimer {
        return CDTimerImpl()
    }
}