package com.kasiumapps.twominutecountdown

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kasiumapps.twominutecountdown.timer.TimerPresenter
import com.kasiumapps.twominutecountdown.timer.TimerView
import com.kasiumapps.twominutecountdown.utils.TimerUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TimerView {

    private var timeLeft = TimerUtils.TWO_MINUTES
    private val savedTimeKey = "savedTimeKey"
    private lateinit var timerPresenter: TimerPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            timeLeft = it.getLong(savedTimeKey, timeLeft)
        }
        setContentView(R.layout.activity_main)
        timerPresenter = TimerPresenter((application as TimerApplication).appComponent.cdTimer(),
            this, applicationContext, timeLeft);

        plus_btn.setOnClickListener {
            timerPresenter.timeLeft = TimerUtils.addTenSeconds(timerPresenter.timeLeft)
            timerPresenter.startCountDownTimer()
        }

        timerPresenter.startCountDownTimer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(savedTimeKey, timerPresenter.timeLeft)
        super.onSaveInstanceState(outState)
    }

    override var textAlignment: Int
        get() = countdown_text.textAlignment
        set(value) {
            countdown_text.textAlignment = value
        }

    override fun setText(text: String) {
        countdown_text.text = text
    }
}
