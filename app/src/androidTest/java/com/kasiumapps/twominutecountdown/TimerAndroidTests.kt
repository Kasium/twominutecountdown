package com.kasiumapps.twominutecountdown

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.kasiumapps.twominutecountdown.timer.CDTimer
import com.kasiumapps.twominutecountdown.timer.TimerPresenter
import com.kasiumapps.twominutecountdown.timer.TimerView
import com.kasiumapps.twominutecountdown.utils.TimerUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */

class TimerAndroidTests {


    private lateinit var targetContext: Context

    @Before
    fun setup() {
        targetContext = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun formatDoneAtZeroMillis() {
        Assert.assertEquals(targetContext.getString(R.string.done), TimerUtils.formatTime(0, targetContext))
    }

    @Test
    fun timerPresenterOnTick() {
        val timeLeft = 1000L
        val timerView = Mockito.mock(TimerView::class.java)
        val timer = Mockito.mock(CDTimer::class.java)

        val timerPresenter = TimerPresenter(timer, timerView, targetContext)
        timerPresenter.onTick(timeLeft-100L)

        Mockito.verify(timerView).setText("00 : 9")
    }

}